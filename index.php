<?php

/**
 * Reminder: This script may not work correctly for customers who do not have the standard number of eyes, 
 * for customers that order multiple lenses at once, 
 * or customers who have ordered contact lenses just once.
 */

require "Customer.php";
require "Order.php";
require "Product.php";
require "Item.php";

function initCustomers($orders, $products)
{
    $response = NULL;

    foreach ($orders as $customerId => $customerOrders) {
        $Customer = new Customer();
        $Customer->id = $customerId;
        $Customer->orders = initOrders($customerOrders, $products);

        $response[] = $Customer;
    }

    return $response;
}

function initOrders($orders, $products): array
{
    $response = [];

    foreach ($orders as $orderDate => $order) {
        $items = [];

        foreach ($order as $item) {
            $Item = new Item();
            $Item->Product = $products[$item[0]];
            $Item->amount = $item[1];
            $Item->power = $item[2];
            $items[] = $Item;
        }

        $OrderObject = new Order();
        $OrderObject->date = DateTime::createFromFormat('Y-m-d|', $orderDate);
        $OrderObject->items = $items;

        $response[] = $OrderObject;
    }

    return $response;
}

function initProducts($goods)
{
    $response = NULL;

    foreach ($goods as $productId => $item) {
        $Product = new Product();
        $Product->id = $productId;
        $Product->expiration = $item;
        $response[$productId] = $Product;
    }

    return $response;
}

$goods = array(
    1 => 180, // Biofinity (6 lenses)
    2 => 90, // Biofinity (3 lenses)
    3 => 30, // Focus Dailies (30)
);



$ordersArray = [
    [
        '2015-04-01' => [
            [1, 2, '-2.00'],
            [1, 2, '-3.00']
        ]
    ],
    [
        '2014-10-01' => [
            [3, 2, '-1.50'],
            [3, 2, '-3.50']
        ],
        '2015-01-01' => [
            [3, 2, '-1.50'],
            [3, 2, '-3.50']
        ],
        '2015-04-15' => [
            [3, 1, '-1.50'],
            [3, 1, '-3.50']
        ]
    ],
    [
        '2014-08-01' => [
            [2, 2, '+0.50']
        ]
    ]
];

$products = initProducts($goods);
$customers = initCustomers($ordersArray, $products);

foreach ($customers as $Customer) {

    $predictedDate = $Customer->calculateLensOrderDatePrediction();

    if ($predictedDate !== NULL) {
        echo "Customer " . $Customer->id . " should order new lenses on " . $predictedDate->format("d.m.Y");
    } else {
        echo "No new order date could be estabilished for customer identified by ID: " . $Customer->id;
    }

    echo "<br />";
}
