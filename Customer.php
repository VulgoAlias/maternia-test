<?php

class Customer
{
    public $id;
    public $orders;

    public function calculateLensOrderDatePrediction(): ?DateTime
    {
        echo "<pre>";
        $response = NULL;
        $lastOrder = $this->getLastOrder();

        if ($lastOrder != NULL) {
            $orderedProducts = $this->getCustomerProducts();
            $expiration = $this->getCustomUseDurations($orderedProducts);

            $response = $lastOrder->date->modify('+' . $expiration . ' days');
        }

        return $response;
    }

    public function getCustomerProducts(): array
    {
        $products = [];

        foreach ($this->orders as $Order) {
            foreach ($Order->items as $Item) {
                if (!array_key_exists($Item->productId, $products)) {
                    $products[$Item->productId] = $Item->amount;
                }
            }
        }

        return $products;
    }

    public function getLastOrder(): ?Order
    {
        $response = NULL;

        foreach ($this->orders as $Order) {

            if ($response === NULL || $Order->date > $response->date) {
                $response = $Order;
            }
        }


        return $response;
    }

    public function getCustomUseDurations($orderedProducts): ?int
    {
        $response = NULL;
        $totalDays = 0; //Per one package
        $datesCount = count($this->orders);

        if ($datesCount > 1) {
            foreach ($this->orders as $key => $Order) {
                if (array_key_exists($key + 1, $this->orders)) {
                    $nextDate = $this->orders[$key + 1]->date;
                    $daysPerProduct = (int) $nextDate->diff($Order->date)->format('%a');
                    $daysPerProduct /= $Order->items[0]->amount; //TODO: Fix for empty orders.
                    $totalDays += $daysPerProduct;
                }
            }

            if (count($orderedProducts) === 1) //Customer uses lenses of the same power
            {
                $totalDays *= 2;
            }

            $response = (int) ($totalDays / $datesCount);
        } elseif ($datesCount === 1) {
            $response = $this->getRecommendedExpirationForOrder($this->orders[0]);
        }

        return $response;
    }

    public function getRecommendedExpirationForOrder(Order $Order)
    {
        $expiration = $Order->items[0]->Product->expiration;
        $productCount = $Order->items[0]->amount;
        $productTypesCount = count($Order->items);

        $response = $expiration * $productCount;

        if ($productTypesCount === 1) {
            $response /= 2;
        }

        return $response;
    }
}
